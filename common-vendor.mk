LAWN_ROOT := /home/saltamimi/android_build/vendor/kuantum/services/Lawnchair
SMS_ROOT := /home/saltamimi/android_build/vendor/kuantum/services/SimpleSMSMessenger
PREFERRED_ROOT := /home/saltamimi/android_build/vendor/kuantum/services/preferred_apps
ROOT_DIR := /home/saltamimi/android_build/vendor/kuantum/services

PRODUCT_PACKAGES += \
    AuroraDroid \
	AuroraServices \
	AuroraStore \
	Bromite \
	BromiteWebview \
	CameraPX \
	com.google.android.maps \
	FakeStore \
	FlorisBoard \
	GmsCore \
	GsfProxy \
	Lawnchair \
	MozillaNlpBackend \
	NominatimNlpBackend \
	SimpleCalculator \
	SimpleCalendar \
	SimpleClock \
	SimpleContacts \
	SimpleDialer \
	SimpleFileManager \
	SimpleGallery \
	SimpleNotes \
	SimpleRecorder \
	SimpleSMSMessenger \
	
PRODUCT_PACKAGE_OVERLAYS += $(LAWN_ROOT)/overlay
PRODUCT_COPY_FILES += \
    $(LAWN_ROOT)/privapp-permissions-lawnchair.xml:$(TARGET_COPY_OUT_SYSTEM_EXT)/etc/permissions/privapp-permissions-lawnchair.xml \
    $(LAWN_ROOT)/lawnchair-hiddenapi-package-whitelist.xml:$(TARGET_COPY_OUT_SYSTEM_EXT)/etc/sysconfig/lawnchair-hiddenapi-package-whitelist.xml \
	$(SMS_ROOT)/privapp-permissions-com.simplemobiletools.smsmessenger.xml:$(TARGET_COPY_OUT_PRODUCT)/etc/permissions/privapp-permissions-com.simplemobiletools.smsmessenger.xml \
	$(PREFERRED_ROOT)/kuantum.xml:$(TARGET_COPY_OUT_PRODUCT)/etc/preferred_apps/kuantum.xml \
	$(ROOT_DIR)/default-permissions-simple-apps.xml:$(TARGET_COPY_OUT_PRODUCT)/etc/default-permissions/default-permissions-simple-apps.xml
